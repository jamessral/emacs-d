;;; Gotta go fast
(when (boundp 'w32-pipe-read-delay)
  (setq w32-pipe-read-delay 0))

(setq-default no-native-compile t)
(setq comp-deferred-compilation nil)

(menu-bar-mode -1)
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

(require 'cl)

(setf gc-cons-threshold 100000000)
(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

;; Package initialize called in early-init
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (setq package-archives '(("melpa" . "https://melpa.org/packages/")
						   ("gnu" . "https://elpa.gnu.org/packages/"))))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-defer t)
(setq use-package-always-ensure t)

(use-package esup
  :ensure t
  :pin melpa)

(use-package auto-compile
  :ensure t
  :init
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))

(use-package better-defaults
  :ensure t)

(use-package diminish
  :ensure t)

;;; Basics
;; Save history of minibuffer
(setq history-length 25)
(savehist-mode)

;; Move through windows with Ctrl-<arrow keys>
(windmove-default-keybindings 'control) ; You can use other modifiers here

;; For help, see: https://www.masteringemacs.org/article/understanding-minibuffer-completion

(setopt enable-recursive-minibuffers t)                ; Use the minibuffer whilst in the minibuffer
(setopt completion-cycle-threshold 1)                  ; TAB cycles candidates
(setopt completions-detailed t)                        ; Show annotations
(setopt tab-always-indent 'complete)                   ; When I hit TAB, try to complete, otherwise, indent
(setopt completion-styles '(basic initials substring)) ; Different styles to match input to candidates

(setopt completion-auto-help 'always)                  ; Open completion always; `lazy' another option
(setopt completions-max-height 20)                     ; This is arbitrary
(setopt completions-detailed t)
(setopt completions-format 'one-column)
(setopt completions-group t)
(setopt completion-auto-select 'second-tab)            ; Much more eage
(keymap-set minibuffer-mode-map "TAB" 'minibuffer-complete) ; TAB acts more like how it does in the shell

(ido-mode)

;; Remember recently edited files
(recentf-mode 1)

;; Revert buffers when underlying file has changed
(global-auto-revert-mode t)

;;; End Basics

(use-package ag
  :ensure t)

(add-to-list 'load-path "./vendor")

;; Borrowed from https://sam217pa.github.io/2016/09/02/how-to-build-your-own-spacemacs/
(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")) ) ; which directory to put backups file
(setq vc-follow-symlinks t )				       ; don't ask for confirmation when opening symlinked file
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)) ) ;transform backups file name
(setq inhibit-startup-screen t )	; inhibit useless and old-school startup screen
(setq ring-bell-function 'ignore )	; silent bell when you make a mistake
(setq coding-system-for-read 'utf-8 )	; use utf-8 by default
(setq coding-system-for-write 'utf-8 )
(setq default-fill-column 80)

;; Don't show native OS scroll bars for buffers because they're redundant
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

(defun jas/reset-ansi-colors (&optional theme)
  "Undo damage caused by some themes"
  (interactive)
  (setq ansi-color-vector [term term-color-black term-color-red term-color-green term-color-yellow term-color-blue term-color-magenta term-color-cyan term-color-white])
  (setq ansi-term-color-vector [term term-color-black term-color-red term-color-green term-color-yellow term-color-blue term-color-magenta term-color-cyan term-color-white]))

(defadvice load-theme
    ;; Make sure to disable current colors before switching
    (before theme-dont-propagate activate)
  (mapc #'disable-theme custom-enabled-themes))

(advice-add 'enable-theme :after #'jas/reset-ansi-colors)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes")
(add-to-list 'load-path "~/.emacs.d/themes")

;; Font
(defun jas/load-font (font-name)
  "Helper to make it easier to switch fonts"
  (interactive)
  (set-face-attribute 'default nil :font font-name))

(defun jas/initialize-fonts ()
  "Fonts setup"
  (interactive)
  (jas/load-font "Source Code Pro")
  (set-face-attribute 'default nil :height 150))

(add-hook 'after-init-hook #'jas/initialize-fonts)

(defun load-dark ()
  "Load Dark Color Scheme."
  (interactive)
  (load-theme 'deeper-blue t))

(defun load-light ()
  "Load Light Color Scheme."
  (interactive)
  (load-theme 'leuven t))

(setq-default default-frame-background-mode 'dark)
(setq default-frame-background-mode 'dark)

(load-dark)

;; Mac key admustments
(setq mac-option-modifier 'control)
(setq mac-command-modifier 'meta)
(add-to-list 'default-frame-alist '(undecorated-round . t))

(global-set-key (kbd "C-h") 'help)
(global-set-key (kbd "M-o") 'other-window)
(global-set-key (kbd "M-\\") 'split-window-right)
(global-set-key (kbd "M-_") 'split-window-below)
;; join line to next line
(defun concat-lines ()
  (interactive)
  (join-line -1))

(global-set-key (kbd "C-j") 'concat-lines)

(defun jas/duplicate-line-below ()
  (interactive)
  (save-excursion
	(let* ((pos-end (line-beginning-position 2))
		   (line    (buffer-substring (line-beginning-position) pos-end)))
	  (goto-char pos-end)
	  (unless (bolp) (newline))
	  (insert line))))

(global-set-key (kbd "C-c C-c") 'recompile)

(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Magit
(use-package magit
  :commands (magit-status)
  :demand t
  :ensure t)
(global-set-key (kbd "C-x g") 'magit-status)

;; Highlights matching parenthesis
(show-paren-mode 1)

(electric-pair-mode 1)

;; Lisp-friendly hippie expand
(setq hippie-expand-try-functions-list
      '(try-expand-dabbrev
         try-expand-dabbrev-all-buffers
         try-expand-dabbrev-from-kill
         try-complete-lisp-symbol-partially
         try-complete-lisp-symbol))

(setq make-backup-files nil) ; stop creating backup~ files
(setq auto-save-default nil) ; stop creating #autosave# files
(setq create-lockfiles nil)

;; Show tabs as 4 spaces
(setq tab-width 4)

;; Use subword mode
(global-subword-mode)
(diminish 'subword-mode)

;; Fix Org Mode syntax stuff
(setq org-src-fontify-natively t)

(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))

(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

(use-package company
  :ensure t
  :demand t
  :diminish company-mode
  :config
  (add-hook 'after-init-hook 'global-company-mode)
  (global-set-key (kbd "C-.") 'company-files)
  (setq company-idle-delay 0.01)
  ;; Disable automatic completions. Ask for it
  ;; (setq company-idle-delay nil)
  ;; Case sensitive company mode
  (setq company-dabbrev-downcase nil)
  :init
  (setq company-backends '(company-files
                           company-keywords
                           company-semantic
                           company-elisp
                           company-yasnippet
                           company-css
                           company-go
                           company-lua)))

(global-set-key (kbd "C-'") 'company-complete)


(global-set-key (kbd "M-/") 'hippie-expand)
(global-set-key (kbd "C-t") 'transpose-chars)

;; Expand Region
(use-package expand-region
  :commands (er/expand-region)
  :ensure t)
(global-set-key (kbd "C-]") 'er/expand-region)

;; Spelling
(when (eq system-type 'windows-nt)
  (setq-default ispell-local-dictionary "en_US")
  (setq ispell-dictionary-alist
        '(("en_US" "[[:alpha:]]" "[^[:alpha:]]" "[']" nil ("-d" "en_US") nil utf-8)))
  (setq ispell-local-dictionary-alist ispell-dictionary-alist))
;; Don't use hard tabs
(setq-default indent-tabs-mode nil)
(add-hook 'before-save-hook #'delete-trailing-whitespace)

;; comments
(defun toggle-comment-on-line ()
  "comment or uncomment current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))
(global-set-key (kbd "C-;") 'toggle-comment-on-line)

;; fix weird os x kill error
(defun ns-get-pasteboard ()
  "Returns the value of the pasteboard, or nil for unsupported formats."
  (condition-case nil
                  (ns-get-selection-internal 'CLIPBOARD)
                  (quit nil)))

(setq electric-indent-mode 1)
(setq-default indent-tabs-mode nil)

;;; LSP
;; Couretsy of https://git.sr.ht/~ashton314/emacs-bedrock/tree/main/item/extras/dev.el
;; (use-package eglot ;; built-in
;;   :hook
;;   ((ruby-mode . eglot-ensure)
;;    (elixir-mode . eglot-ensure)
;;    (js2-mode . eglot-ensure)
;;    (go-mode . eglot-ensure)
;;    (rust-mode . eglot-ensure)
;;    (lua-mode . eglot-ensure)
;;    (typescript-mode . eglot-ensure)
;;    (typescriptreact-mode . eglot-ensure))
;;   :custom
;;   (eglot-extend-to-xref t)
;;   :config
;;   (fset #'jsonrcp--log-event #'ignore)
;;   :init
;;   (with-eval-after-load 'eglot
;; 	(add-to-list 'eglot-server-programs
;; 				 '(ruby-mode . ("ruby-lsp" "")))
;; 	(add-to-list 'eglot-server-programs
;; 				 '(elixir-mode . ("elixir-ls" "")))))
;; End LSP

;; (use-package flycheck
;;   :ensure t
;;   :diminish 'flycheck-mode
;;   :config
;;   (global-set-key (kbd "C-c ! v") 'flycheck-verify-setup)
;;   (add-hook 'after-init-hook #'global-flycheck-mode)
;;   (add-hook 'after-init-hook
;;             (lambda ()
;;               (flycheck-add-mode 'ruby-rubocop 'ruby-mode)))
;;   (setq-default flycheck-temp-prefix ".flycheck")
;;   ;; disable jshint snce we prefer eslint checking
;;   (setq-default flycheck-disabled-checkers
;;                 (append flycheck-disabled-checkers
;; 			'(javascript-jshint))))

;;; Some Bascis
(use-package exec-path-from-shell
  :commands (exec-path-from-shell-initialize exec-path-from-shell-copy-env)
  :ensure t
  :init
  (when (memq window-system '(mac ns x))
    (exec-path-from-shell-initialize)
    (exec-path-from-shell-copy-envs
     '("PATH" "ANDROID_HOME"))))

;;; End Basics

;;; Javascript
(use-package add-node-modules-path
  :after js2-mode
  :ensure t)

(use-package js2-mode
  :ensure t
  :init
  (setq js-indent-level 2)
  (setq js2-basic-offset 2)
  ;;; JS-mode and derivatives
  ;; Turn off js2 mode errors & warnings (we lean on eslint/standard)
  ;; this also affects rjsx mode (yay!)
  (setq js2-mode-show-parse-errors nil)
  (setq js2-mode-show-strict-warnings nil)
  ;; (setq js2-basic-offset 2)
  ;; js-mode (which js2 is based on) binds "M-." which conflicts with xref, so
  ;; unbind it.
  (add-hook 'js2-mode-hook (lambda ()
                             (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t))))

(use-package web-mode
  ;; :after (flycheck)
  :ensure t
  :init
  (setq web-mode-script-padding 0)
  (setq web-mode-style-padding 0)
  (setq web-mode-markup-indent-offset 2)
  (setq-default indent-tabs-mode nil)
  (add-hook 'web-mode-hook #'add-node-modules-path)
  ; don't auto-insert quotes for attributes
  (setq web-mode-enable-auto-quoting nil)
  :config
  ;; (flycheck-add-mode 'javascript-eslint 'web-mode)
  (setq indent-tabs-mode nil))

(eval-after-load 'js2-mode
  '(add-hook 'js2-mode-hook #'add-node-modules-path))

;; (defun codefalling/reset-eslint-rc ()
;;   (let ((rc-path (if (project-project-p)
;;                      (concat (project-project-root) ".eslintrc"))))
;;     (if (file-exists-p rc-path)
;;         (progn
;;           (message rc-path)
;;           (setq flycheck-eslintrc rc-path)))))


(defun my/set-web-mode-indent ()
  (interactive)
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-css-indent-offset 2)
  ;; Don't let web mode try to line things up strangely in js
  (add-to-list 'web-mode-indentation-params '("lineup-args" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-calls" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-concats" . nil))
  (add-to-list 'web-mode-indentation-params '("lineup-ternary" . nil)))

(add-hook 'web-mode-hook 'my/set-web-mode-indent)

;; always use jsx mode for JS
(setq web-mode-content-types-alist '(("jsx"  . "\\.js[x]?\\'")))
(use-package json-mode
  :ensure t)

(add-to-list 'auto-mode-alist '("\\.json\\'" . json-mode))

(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html\\.erb\\'" . my/setup-erb))
(add-to-list 'auto-mode-alist '("\\.heex\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . my/setup-erb))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-hook 'js2-mode-hook #'js2-imenu-extras-mode)

(use-package xref-js2
  :defer t
  :ensure t)

;;; End Javascript

;;; Ruby
(use-package ruby-mode
  :ensure t)

(use-package ruby-end
  :after ruby-mode
  :diminish ruby-end-mode
  :ensure t)

(use-package inf-ruby
  :after ruby-mode
  :ensure t)

(use-package minitest
  :after ruby-mode
  :ensure t)

(add-hook 'ruby-mode-hook (lambda ()
                            (progn
                              (ruby-end-mode))))

(defun my/setup-erb ()
  (interactive)
  (web-mode)
  ;; (flycheck-mode -1)
  (setq indent-tabs-mode nil))
;;; End Ruby

;;; Typescript
(use-package typescript-mode
  :ensure t
  :init
  (setq typescript-indent-level 2)
  :config
  ;; we choose this instead of tsx-mode so that eglot can automatically figure out language for server
  ;; see https://github.com/joaotavora/eglot/issues/624 and https://github.com/joaotavora/eglot#handling-quirky-servers
  (define-derived-mode typescriptreact-mode typescript-mode
    "TypeScript TSX")

  (add-hook 'typescript-mode-hook #'add-node-modules-path)
  (add-hook 'typescriptreact-mode-hook #'add-node-modules-path)
  ;; (flycheck-add-mode 'javascript-eslint 'typescript-mode)
  ;; (flycheck-add-mode 'javascript-eslint 'typescriptreact-mode)

  ;; use our derived mode for tsx files
  (add-to-list 'auto-mode-alist '("\\.tsx?\\'" . typescriptreact-mode))
  ;; by default, typescript-mode is mapped to the treesitter typescript parser
  ;; use our derived mode to map both .tsx AND .ts -> typescriptreact-mode -> treesitter tsx
)

;;; End Typescript

;; Coffeescript
(use-package coffee-mode
  :ensure t)
;;; End Coffeescript

;;; SCSS
(use-package sass-mode
  :ensure t)
;;; END SCSS

(use-package yaml-mode
  :defer t
  :ensure t)

(use-package dumb-jump
  :ensure t
  :init
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

;;; OrgMode
(use-package org
  :ensure t
  :config
  (setq org-agenda-files (list "~/org/work.org"
							   "~/org/personal.org"))
  (setq org-capture-templates
      '(("W" "Work todo" entry (file+headline "~/org/work.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
		("P" "Personal todo" entry (file+headline "~/org/personal.org" "Tasks")
         "* TODO %?\n  %i\n  %a")
        ("w" "Work Notes" entry (file+datetree "~/org/work.org")
         "* %?\nEntered on %U\n  %i\n  %a")
		("p" "Personal Notes" entry (file+datetree "~/org/personal.org")
         "* %?\nEntered on %U\n  %i\n  %a"))))

(use-package org-bullets
  :ensure t)
(add-hook 'org-mode-hook (lambda ()
                           (org-bullets-mode 1)))


(defun jas/go-to-file-in-split (filepath)
  "Open file in split.  FILEPATH is an absolute path to file."
  (interactive)
  (split-window-right)
  (windmove-right)
  (find-file filepath))

(defun jas/go-to-work-org-file ()
  "Edit my work org file."
  (interactive)
  (jas/go-to-file-in-split "~/org/work.org"))

(defun jas/go-to-personal-org-file ()
  "Edit my personal org file."
  (interactive)
  (jas/go-to-file-in-split "~/org/personal.org"))

;;; End OrgMode

;;; Utils
(defun jas/insert-todo ()
  "Insert Todo comment."
  (interactive)
    (insert "TODO(jsral): ")
    (toggle-comment-on-line))

(defun jas/insert-note ()
  "Insert Note comment."
  (interactive)
  (insert "NOTE(jsral): ")
  (toggle-comment-on-line))
;;; End Utils

;; Git Gutter Fringe
(use-package git-gutter-fringe+
  :ensure t
  :diminish t
  :init
  (global-git-gutter+-mode t))
;;; Golang
(use-package go-autocomplete
  :ensure t)

(use-package company-go
  :ensure t)

(use-package gotest
  :ensure t)

(use-package go-mode
  :ensure t
  :config
  ;; (go-eldoc-setup)
  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "C-c C-c") 'recompile)
  (setq gofmt-command "goimports")
  (setq tab-width 4)
										; Call Gofmt before saving
  (add-hook 'before-save-hook 'gofmt-before-save)
										; Customize compile command to run go build
  (if (not (string-match "go" compile-command))
      (set (make-local-variable 'compile-command)
           "go generate && go build -v && go test -v && go vet"))
										; Godef jump key binding
  (local-set-key (kbd "M-.") 'godef-jump)
  (local-set-key (kbd "M-*") 'pop-tag-mark)
  (add-hook 'go-mode-hook (lambda ()
							(set (make-local-variable 'company-backends) '(company-go))
							(company-mode))))
;;; End Golang

;;; Rust
(use-package rust-mode
  :ensure t)
;;; End Rust

;;; C/C++
(setq-default c-default-style "linux"
      tab-width 4
      indent-tabs-mode t
      c-basic-offset 4)
(setq-default c-basic-offset 4)

(add-hook 'c-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-c C-c") 'recompile)))
(add-hook 'c++-mode-hook
		  (lambda ()
			(local-set-key (kbd "C-c C-c") 'recompile)))
;; End C/C++

;;; Work functions
(defun jas/www-dev ()
  "Run all processes needed for Extend Dev"
  (interactive)
  (let ((current-dir default-directory))
    (cd "~/code/bark-www")
    (async-shell-command "bin/rails s" "*Rails*")
    (async-shell-command "bundle exec sidekiq -q priority -q analysis -q screen_time -q default -q slow" "*Sidekiq*")
    (cd current-dir)))
(defun jas/admin-dev ()
  "Run all processes needed for Extend Dev"
  (interactive)
  (let ((current-dir default-directory))
    (cd "~/code/bark-admin")
    (async-shell-command "bin/rails s" "*Rails*")
    (async-shell-command "bundle exec sidekiq -q priority -q analysis -q screen_time -q default -q slow" "*Sidekiq*")
    (cd current-dir)))
;;; End Work Functions
(put 'narrow-to-region 'disabled nil)
(setq css-indent-offset 2)

;;; UI
;; Turn off the menu bar at the top of each frame because it's distracting
(menu-bar-mode -1)
(tool-bar-mode -1)
(global-hl-line-mode -1)
(global-display-line-numbers-mode t)
;; Font
(set-face-attribute 'default nil :font "Source Code Pro")
(set-face-attribute 'default nil :height 150)

;; Add some margins
(setq-default left-margin-width 2 right-margin-width 2)
(setq-default header-line-format "")

;; Customize mode-line
(setq mode-line-format
      (list
        " "
        mode-line-mule-info
        mode-line-modified
        mode-line-frame-identification
        mode-line-buffer-identification
        " "
        mode-line-position
        vc-mode
        " "
        mode-line-modes))


(setq system-uses-terminfo nil)
(prefer-coding-system 'utf-8)

(add-to-list 'comint-output-filter-functions 'ansi-color-process-output)
(ansi-color-for-comint-mode-on)

(display-time-mode 1)

(setq linum-format " %d ")

(setq linum-relative-current-symbol "")

;; Set default font

;; These settings relate to how emacs interacts with your operating system
(setq ;; makes killing/yanking interact with the clipboard
  x-select-enable-clipboard t

  ;; I'm actually not sure what this does but it's recommended?
  x-select-enable-primary t

  ;; Save clipboard strings into kill ring before replacing them.
  ;; When one selects something in another program to paste it into Emacs,
  ;; but kills something in Emacs before actually pasting it,
  ;; this selection is gone unless this variable is non-nil
  save-interprogram-paste-before-kill t

  ;; Shows all options when running apropos. For more info,
  ;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Apropos.html
  apropos-do-all t

  ;; Mouse yank commands yank at point instead of at click.
  mouse-yank-at-point t)

;; No cursor blinkning, it's distracting
;; (blink-cursor-mode 0)

;; full path in title bar ()
(setq-default frame-title-format "%b (%f)")

;; don't pop up font menu
(global-set-key (kbd "s-t") '(lambda () (interactive)))

;; no bell
(setq ring-bell-function 'ignore)
;;; End UI

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(header-line ((t (:box nil)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("e27c9668d7eddf75373fa6b07475ae2d6892185f07ebed037eedf783318761d7"
	 default))
 '(package-selected-packages nil))
