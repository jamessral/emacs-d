;;; early-init.el --- early bird  -*- no-byte-compile: t -*-
(require 'package)
(setq load-prefer-newer t)
(setq-default no-native-compile t)
(setq comp-deferred-compilation nil)
;;(package-initialize)
;;; early-init.el ends here
